#pragma once
#include <stdint.h>

class IPriceable
{
public:
	virtual uint8_t getVat() const = 0;
	virtual float getPrice() const = 0;
};
