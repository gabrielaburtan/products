#include "Products.h"
#include <vector>
#include <iostream>
#include <algorithm>
Product::Product(uint16_t id, const std::string& name, float price) :
	m_id(id), m_name(name), m_rawPrice(price)
{
	// Empty
}

float Product::getRawPrice()
{
	return m_rawPrice;
}

std::string Product::getName()
{
	return m_name;
}

uint16_t Product::getID()
{
	return m_id;
}



