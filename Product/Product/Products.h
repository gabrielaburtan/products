#pragma once
#include <stdint.h>
#include <string>
#include <vector>


class Product
{
public:
	Product(uint16_t id, const std::string& name, float price);
	float getRawPrice();
	std::string getName();
	uint16_t getID();

private:
	uint16_t m_id;
	std::string m_name;
	float m_rawPrice;
};
