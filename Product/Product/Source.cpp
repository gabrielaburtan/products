#include "Products.h"
#include <fstream>
#include <vector>

int main()
{
	std::vector<Product> products;
	uint16_t id;
	std::string name;
	float price;
	uint16_t vat;
	std::string dateOrType;
	for (std::ifstream productsFile("Products.proddb"); !productsFile.eof();)
	{
		productsFile >> id >> name >> price >> vat >> dateOrType;
		// validate input...
		products.push_back(Product(id, name, price, vat, dateOrType));
	}
	Product::printNonperishableProducts(products);
	Product::sortPrint(products);
	return 0;
}